/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: "#1a45e5",
        secondary: "#f80744",
        error: "#f80744",
        lightred: "rgb(254 226 226 / var(--tw-bg-opacity))",
      },
    },
  },
  plugins: [],
};
