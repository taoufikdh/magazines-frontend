/* eslint-disable @typescript-eslint/no-explicit-any */
import { lazy } from "react";
import {
  Navigate,
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Layout from "./components/Layout";
const Magazines = lazy(() => import("./views/magazines/Magazines"));
const Profile = lazy(() => import("./views/Profile"));
const Subscriptions = lazy(() => import("./views/subscription/Subscriptions"));
const Login = lazy(() => import("./views/auth/Login"));

const Routes = () => {
  const isAuthenticated = true; // Replace this with your authentication logic

  const privateRoutes = () => {
    return {
      element: <Layout />,
      children: [
        { path: "/", element: <Magazines /> },
        { path: "/subscriptions", element: <Subscriptions /> },
        { path: "/profile", element: <Profile /> },

        { path: "*", element: <Navigate to="/" replace /> },
      ],
    };
  };
  const publicRoutes = () => {
    return [
      { path: "/login", element: <Login /> },
      { path: "*", element: <Navigate to="/login" replace /> },
    ];
  };

  const router = createBrowserRouter([
    isAuthenticated ? privateRoutes() : {},
    ...publicRoutes(),
  ]);
  // Provide the router configuration using RouterProvider
  return <RouterProvider router={router} />;
};
export default Routes;
