/* eslint-disable @typescript-eslint/no-explicit-any */
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { queriesKeys } from "./querykeys";
import { request } from "../../utils/axios";
import { Magazine, MagazineInputData } from "../../views/magazines/type";
import { USER_ID } from "../../utils/constants";
import { SubscriptionResponse } from "../../views/subscription/type";

export type QueryOptions = {
  enabled?: boolean;
};

const fetchMagazines = () => {
  const url = `${import.meta.env.VITE_MAGAZINES_BASE_URL}/api/magazines`;
  return request<object, Magazine[]>({
    url,
    method: "get",
  });
};

const fetchSubscriptions = () => {
  const url = `${
    import.meta.env.VITE_MAGAZINES_BASE_URL
  }/api/subscriptions/${USER_ID}`;

  return request<object, SubscriptionResponse[]>({
    url,
    method: "get",
  });
};
const subscibeToMagazine = ({ magazineId }: { magazineId: string }) => {
  console.log({ magazineId });

  const url = `${
    import.meta.env.VITE_MAGAZINES_BASE_URL
  }/api/subscriptions/1/${magazineId}`;
  return request<object, Magazine[]>({
    url,
    method: "post",
  });
};
const deleteMagazine = ({ magazineId }: { magazineId: string }) => {
  console.log({ magazineId });

  const url = `${
    import.meta.env.VITE_MAGAZINES_BASE_URL
  }/api/magazines/${magazineId}`;
  return request<object, void>({
    url,
    method: "delete",
  });
};
const addMagazine = (data: MagazineInputData) => {
  console.log({ data });

  const url = `${import.meta.env.VITE_MAGAZINES_BASE_URL}/api/magazines`;
  return request<object, void>(
    {
      url,
      method: "post",
    },
    data
  );
};

export const useMagazinesData = ({ enabled }: QueryOptions) => {
  return useQuery({
    queryKey: [queriesKeys.MAGAZINES],
    queryFn: fetchMagazines,
    enabled,
  });
};

export const useAddMagazine = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: addMagazine,
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: [queriesKeys.MAGAZINES],
      });
    },
  });
};

export const useDeleteMagazine = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: deleteMagazine,
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: [queriesKeys.MAGAZINES],
      });
    },
  });
};

export const useSubscribeToMagazine = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: subscibeToMagazine,
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: [queriesKeys.SUBSCRIPTIONS],
      });
    },
  });
};
export const useSubscriptionsData = ({ enabled }: QueryOptions) => {
  return useQuery({
    queryKey: [queriesKeys.MAGAZINES],
    queryFn: fetchSubscriptions,
    enabled,
  });
};
