import { ActionType, Magazine } from "../../views/magazines/type";
import Button from "../common/Button";
type Props = {
  tableHead: string[];
  tableRows: Magazine[];
  title: string;
  actionsInfo?: ActionType[];
};

function MagazineList({ tableHead, tableRows, title, actionsInfo }: Props) {
  return (
    <>
      <div className="font-bold text-sm p-5">{title}</div>
      <table className="min-w-full divide-y divide-gray-200 w-full">
        <thead className="bg-gray-50">
          <tr>
            {tableHead.map((head, index) => (
              <th
                key={index.toString()}
                scope="col"
                className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
              >
                {head}
              </th>
            ))}
          </tr>
        </thead>
        <tbody className="divide-y divide-gray-200">
          {tableRows.length === 0 ? (
            <div className="flex text-gray-400 text-center mt-10 mx-20">
              No result
            </div>
          ) : (
            tableRows.map((row) => (
              <tr key={row.id}>
                <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                  {row.id}
                </td>
                <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                  {row.title}
                </td>
                <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                  {row.description}
                </td>
                {actionsInfo?.map((action) => (
                  <td className="px-6 py-4 text-sm font-medium text-right whitespace-nowrap">
                    <Button
                      label={action.label}
                      variant={action.type}
                      onClick={() => action.onClick(row.id)}
                    />
                  </td>
                ))}
              </tr>
            ))
          )}
        </tbody>
      </table>
    </>
  );
}

export default MagazineList;
