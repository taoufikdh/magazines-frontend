import { ReactNode } from "react";

type Props = {
  children: ReactNode;
};

function Card({ children }: Props) {
  return (
    <div className="w-[800px] rounded-lg overflow-hidden bg-red-100 shadow-lg p-5 	">
      {children}
    </div>
  );
}

export default Card;
