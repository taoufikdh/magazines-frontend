import Button from "./Button";

interface ConfirmDialogProps {
  isOpen: boolean;
  onClose: () => void;
  onConfirm: () => void;
  title: string;
  description: string;
}

const ConfirmDialog = ({
  isOpen,
  onClose,
  onConfirm,
  title,
  description,
}: ConfirmDialogProps) => {

  return (
    <>
      {isOpen && (
        <div
          className="fixed inset-0 z-50 flex items-center justify-center bg-gray-800 bg-opacity-50"
          onClick={onClose}
        >
          <div className="bg-white p-8 rounded-lg shadow-lg  flex-colmun items-center justify-center ">
            <h2 className="text-xl font-semibold mb-4 text-center">{title}</h2>
            <p className="text-sm text-gray-500 text-center mb-10 w-60 ">
              {description}
            </p>
            <div className="flex justify-end">
              <Button label="Cancel" variant="secondary" onClick={onClose} />
              <Button label="Confirm" onClick={onConfirm} className="ml-2" />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ConfirmDialog;
