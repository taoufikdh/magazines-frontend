import React from "react";

interface ButtonProps {
  variant?: "primary" | "secondary" | "tertiary";
  onClick?: () => void;
  loading?: boolean;
  label: string;
  className?: string;
}

const Button: React.FC<ButtonProps> = ({
  variant = "primary",
  onClick,
  loading = false,
  label,
  className,
}) => {
  console.log({ variant });

  const buttonStyle =
    variant === "primary"
      ? "bg-primary"
      : variant === "secondary"
      ? "bg-secondary"
      : "bg-transparent border border-blue-500 hover:border-blue-600 text-blue-500 hover:text-white";

  return (
    <button
      onClick={onClick}
      disabled={loading}
      className={`w-50 flex items-center justify-center relative py-2 px-4 rounded-md text-white ${buttonStyle} ${
        loading ? "cursor-not-allowed" : ""
      }${className ? className : ""}`}
    >
      <span className={loading ? "invisible" : "visible"}>{label}</span>
    </button>
  );
};

export default Button;
