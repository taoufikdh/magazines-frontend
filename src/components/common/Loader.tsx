import React, { ReactNode } from "react";

interface LoaderProps {
  size?: "small" | "medium" | "large";
  isLoading: boolean;
  children: ReactNode;
  width?: string;
}

const Loader: React.FC<LoaderProps> = ({
  size = "medium",
  isLoading,
  children,
}) => {
  const spinnerSizeClasses: Record<string, string> = {
    small: "h-2 w-2",
    medium: "h-12 w-12",
    large: "h-16 w-16",
  };

  const spinnerSizeClass = spinnerSizeClasses[size];

  return (
    <div className={`flex justify-center items-center w-ful`}>
      {isLoading ? (
        <div className="relative">
          <div
            className={`${spinnerSizeClass} absolute rounded-full border-4 border-gray-200 `}
          ></div>
          <div
            className={`${spinnerSizeClass} absolute rounded-full border-4 border-gray-500 animate-spin border-t-transparent`}
          ></div>
        </div>
      ) : (
        children
      )}
    </div>
  );
};

export default Loader;
