import { useState } from "react";

interface Tab {
  title: string;
  content: JSX.Element;
}

interface TabProps {
  tabs: Tab[];
}

const Tabs = ({ tabs }: TabProps) => {
  const [activeTab, setActiveTab] = useState(0);

  const handleClick = (index: number) => {
    setActiveTab(index);
  };

  return (
    <div className="border border-gray-200 rounded-lg w-2/4 bg-red-200 ">
      <div className="flex w-full">
        {tabs.map((tab, index) => (
          <button
            key={index}
            onClick={() => handleClick(index)}
            className={`py-2 px-4 ${
              activeTab === index
                ? "bg-gray-800 text-white"
                : "bg-gray-200 text-gray-800"
            } rounded-t-lg focus:outline-none`}
          >
            {tab.title}
          </button>
        ))}
      </div>
      <div className="p-4 w-full">{tabs[activeTab].content}</div>
    </div>
  );
};

export default Tabs;
