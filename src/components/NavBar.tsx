// const Navbar = () => {
//   return (
//     <div className="w-full h-[65px] fixed top-0 shadow-lgb g-[#fcfbfe17] backdrop-blur-md z-50 px-10">
//       <div className="w-full h-full flex flex-row items-center justify-between m-auto px-[10px]">
//         <div className="font-bold text-lg">Magazine App</div>
//         <div className="bg-gray-500 p-3 text-white rounded-full"> TD</div>
//       </div>
//     </div>
//   );
// };

import { Link } from "react-router-dom";

// export default Navbar;

const Navbar = () => {
  return (
    <nav className="bg-white-500 shadow-lg fixed h-[65px] backdrop-blur-md z-2 px-10 top-0 right-0 left-0">
      <div className="container mx-auto px-4 py-2 flex justify-between items-center">
        <div className="flex items-center">
          <h1 className="text-xl font-bold">Digital Magazin</h1>
        </div>
        <ul className="mt-4 flex justify-between items-center  w-1/3">
          <li>
            <Link to="/magazines" className="text-gray-800 hover:text-primary">
              Magazines
            </Link>
          </li>
          <li className="">
            <Link
              to="/subscriptions"
              className="text-gray-800 hover:text-primary"
            >
              Subscriptions
            </Link>
          </li>
          <li className="">
            <Link to="/profile" className="text-gray-800 hover:text-primary">
              Profile
            </Link>
          </li>
        </ul>
        <div className="bg-gray-500 p-3 text-white rounded-full"> TD</div>
      </div>
    </nav>
  );
};

export default Navbar;
