import { Subscription } from "../../views/subscription/type";
type Props = {
  tableHead: string[];
  tableRows: Subscription[];
  title: string;
};

function SusbcriptionList({ tableHead, tableRows }: Props) {
  return (
    <table className="min-w-full divide-y divide-gray-200">
      <thead className="bg-gray-50">
        <tr>
          {tableHead.map((head, index) => (
            <th
              key={index.toString()}
              scope="col"
              className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
            >
              {head}
            </th>
          ))}
        </tr>
      </thead>
      <tbody className="divide-y divide-gray-200">
        {tableRows.map((row) => (
          <tr key={row.id}>
            <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
              {row.id}
            </td>
            <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
              {row.magazineName}
            </td>
            <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
              {row.price}
            </td>
            <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
              {row.startDate}
            </td>{" "}
            <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
              {row.endDate}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default SusbcriptionList;
