export const FETCH_STATUS = {
  fetching: "fetching",
  loading: "loading",
  idle: "idle",
};
export const STANDARD_DATE_FORMAT = "yyyy-MM-dd";

// hard coded the user id for simplicity because auth is not implement(matter of time and deadline)
export const USER_ID = 1;
