import Routes from "./Routes";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

const queryClient = new QueryClient();

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <div className="mt-[150px] flex justify-center">
        <Routes />
      </div>
    </QueryClientProvider>
  );
};

export default App;
