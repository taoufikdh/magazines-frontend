export type Magazine = {
  id: string;
  title: string;
  description?: string;
  price: string;
};

export type MagazineInputData = {
  title: string;
  price: string;
  description: string;
};
export type ActionType = {
  label: string;
  type: "primary" | "secondary";
  onClick: (id: string) => void;
};
