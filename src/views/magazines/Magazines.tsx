import { useCallback, useMemo, useState } from "react";
import Card from "../../../src/components/common/Card";
import ConfirmDialog from "../../../src/components/common/ConfirmDIalog";
import {
  useDeleteMagazine,
  useMagazinesData,
  useSubscribeToMagazine,
} from "../../../src/hooks/api/useApiData";
import Loader from "../../components/common/Loader";
import { FETCH_STATUS } from "../../utils/constants";
import { ActionType } from "./type";
import Button from "../../components/common/Button";
import AddMagazineDialog from "../../components/magazine/AddMagazineDialog";
import MagazineList from "../../components/magazine/MagazineList";
const tableHead = ["id", "name", "description", "", ""];

function Magazines() {
  const [openSubscriptionDialog, setOpenSubscriptionDialog] = useState(false);
  const [openDeletionDialog, setOpenDeletionDialog] = useState(false);
  const [openAddMagazineDialog, setOpenAddMagazine] = useState(false);
  setOpenAddMagazine;
  const [magazineId, setMagazineId] = useState<string | null>(null);
  const onClose = useCallback(() => {
    setOpenSubscriptionDialog(false);
    setOpenDeletionDialog(false);
  }, []);
  const { mutateAsync: subscribeToMagazine } = useSubscribeToMagazine();
  const { mutateAsync: deleteMagazine } = useDeleteMagazine();

  const onConfirmSubscription = () => {
    subscribeToMagazine({ magazineId: magazineId ?? "" })
      .then(() => {
        alert("Subscription successful!");
        setMagazineId(null);
        setOpenSubscriptionDialog(false);
      })
      .catch(() => {
        setMagazineId(null);
        setOpenSubscriptionDialog(false);
      });
  };
  const onConfirmDeletion = () => {
    deleteMagazine({ magazineId: magazineId ?? "" })
      .then(() => {
        setMagazineId(null);
        setOpenDeletionDialog(false);
        alert("Deletion successful!");
      })
      .catch(() => {
        setMagazineId(null);
        setOpenDeletionDialog(false);
      });
  };

  const onSubscribe = useCallback((magazineId: string) => {
    setOpenSubscriptionDialog(true);
    setMagazineId(magazineId);
  }, []);

  const onDelete = useCallback((magazineId: string) => {
    setOpenDeletionDialog(true);
    setMagazineId(magazineId);
  }, []);

  const { data, fetchStatus } = useMagazinesData({
    enabled: true,
  });

  const actionsInfo: ActionType[] = useMemo(() => {
    return [
      {
        label: "Subscribe",
        type: "primary",
        onClick: onSubscribe,
      },
      {
        label: "Delete",
        type: "secondary",
        onClick: onDelete,
      },
    ];
  }, [onDelete, onSubscribe]);

  const openAddMagazineDialogFn = () => {
    setOpenAddMagazine(true);
  };

  return (
    <Loader isLoading={fetchStatus === FETCH_STATUS.fetching}>
      <Card>
        <Button label="+ Add" onClick={openAddMagazineDialogFn} />
        <MagazineList
          tableHead={tableHead}
          tableRows={data?.data ?? []}
          title="Magazines"
          actionsInfo={actionsInfo}
        />
        <ConfirmDialog
          isOpen={openSubscriptionDialog}
          onClose={onClose}
          onConfirm={onConfirmSubscription}
          title="Subscribe to Magazine"
          description=" You are about to subscribe to this magazine. Are you sure you want
          to proceed?"
        />
        <ConfirmDialog
          isOpen={openDeletionDialog}
          onClose={onClose}
          onConfirm={onConfirmDeletion}
          title="Delete Magazine"
          description=" You are about to delete this magazine. Are you sure you want
          to proceed?"
        />
        <AddMagazineDialog
          open={openAddMagazineDialog}
          onClose={() => setOpenAddMagazine(false)}
        />
      </Card>
    </Loader>
  );
}

export default Magazines;
