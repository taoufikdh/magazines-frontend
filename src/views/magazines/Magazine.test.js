/* eslint-disable no-undef */
import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import Magazines from "./Magazines";

jest.mock("../../../src/hooks/api/useApiData", () => ({
  useDeleteMagazine: jest.fn(),
  useMagazinesData: jest.fn(() => ({
    data: { data: [] },
    fetchStatus: "success",
  })),
  useSubscribeToMagazine: jest.fn(),
}));

describe("Magazines Component", () => {
  test("renders without crashing", () => {
    render(<Magazines />);
  });

  test('opens subscription dialog when "Subscribe" button is clicked', async () => {
    const { getByText } = render(<Magazines />);
    fireEvent.click(getByText("+ Add"));

    // Confirm the subscription dialog is opened
    await waitFor(() => {
      expect(getByText("Subscribe to Magazine")).toBeInTheDocument();
    });
  });

  test('opens deletion dialog when "Delete" button is clicked', async () => {
    const { getByText } = render(<Magazines />);
    fireEvent.click(getByText("+ Add"));

    // Confirm the deletion dialog is opened
    await waitFor(() => {
      expect(getByText("Delete Magazine")).toBeInTheDocument();
    });
  });

  // You can write more tests to cover other functionalities of the component
});
