import { format } from "date-fns";
import { Subscription } from "../subscription/type";
import { STANDARD_DATE_FORMAT } from "../../utils/constants";

/* eslint-disable @typescript-eslint/no-explicit-any */
export const mapToSuscriptionData = (subscription: any): Subscription => {
  return {
    id: subscription.id,
    price: subscription.magazine.price,
    magazineName: subscription.magazine.title,
    startDate: format(subscription.startDate, STANDARD_DATE_FORMAT),
    endDate: format(subscription.endDate, STANDARD_DATE_FORMAT),
  };
};
