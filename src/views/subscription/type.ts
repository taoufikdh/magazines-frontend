export type Subscription = {
  id: string;
  price: string;
  magazineName: string;
  startDate: string;
  endDate: string;
};

export type SubscriptionResponse = {
  id: string;
  user: Record<string, string>;
  magazine: Record<string, string>;
  endDate: string;
  startDate: string;
};
