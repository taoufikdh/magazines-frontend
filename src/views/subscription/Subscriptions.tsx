import { useMemo } from "react";
import Loader from "../../components/common/Loader";
import Tabs from "../../components/common/Tab";
import SusbcriptionList from "../../components/subscriptions/SubscriptionList";
import { useSubscriptionsData } from "../../hooks/api/useApiData";
import { FETCH_STATUS } from "../../utils/constants";
import { mapToSuscriptionData } from "../magazines/mappers";

const tableHead = ["id", "Magazine", "Price", "Start Date", "End Date"];

function Subscriptions() {
  const { data, fetchStatus } = useSubscriptionsData({ enabled: true });

  const subscriptions = useMemo(
    () =>
      data?.data
        .filter((sub) => sub.magazine)
        .map((subsription) => mapToSuscriptionData(subsription)),
    [data?.data]
  );
  const tabs = [
    {
      title: "Current Subscriptions ",
      content: (
        <Loader isLoading={fetchStatus === FETCH_STATUS.fetching}>
          <SusbcriptionList
            tableHead={tableHead}
            tableRows={subscriptions ?? []}
            title="Current Subscriptions"
          />
        </Loader>
      ),
    },
    {
      title: "Past Subscriptions",
      content: (
        <Loader isLoading={fetchStatus === FETCH_STATUS.fetching}>
          <SusbcriptionList
            tableHead={tableHead}
            tableRows={subscriptions ?? []}
            title="Past Subscriptions"
          />
        </Loader>
      ),
    },
  ];

  return <Tabs tabs={tabs} />;
}

export default Subscriptions;
