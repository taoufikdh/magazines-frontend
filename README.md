# Digital Magazine Subscription Application

Welcome to the Digital Magazine Subscription Application! This application allows users to subscribe to digital magazines for a monthly price. Users can explore available magazines, manage their subscriptions, and view their profile information.

## Features

- Magazine subscriptions: Users can browse available magazines and subscribe to them.
- Subscription management: Users can manage their subscriptions, including viewing active subscriptions and canceling them.

## Tech Stack

This project is built using the following technologies:

- **React.js**: A JavaScript library for building user interfaces.
- **Vite**: A fast, opinionated web development build tool that serves your code via native ES Module imports during development.
- **Tailwind CSS**: A utility-first CSS framework for creating custom designs quickly.
- **React Query**: A library for managing, caching, synchronizing, and updating server state in React applications.

## Getting Started

To get started with the Digital Magazine Subscription Application, follow these steps:

1. Clone the repository:

   ```bash
   git clone <repository_url>
   ```

2. install dependecies

   ```bash
   npm i
   ```

3. run project

```bash
npm run dev
```
